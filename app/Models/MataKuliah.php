<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MataKuliah extends Model
{
    use HasFactory;
    protected $table = "mata_kuliah";
    protected $guarded = [];

    public function dosen()
    {
        return $this->hasOne(User::class, 'id', 'id_dosen');
    }

    public function ruangKelas()
    {
        return $this->hasOne(RuangKelas::class, 'id', 'id_ruangan_kelas');
    }

    public function hari()
    {
        return $this->hasOne(Hari::class, 'id', 'id_hari');
    }
}
