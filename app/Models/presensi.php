<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Presensi extends Model
{
    use HasFactory;

    public function master_presensi()
    {
        return $this->hasOne(MasterPresensi::class, 'id', 'id_master_presensi');
    }
}
