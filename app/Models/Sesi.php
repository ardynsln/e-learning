<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sesi extends Model
{
    use HasFactory;
    protected $table = "sesi";
    protected $guarded = [];


    public function status_tugas($id_sesi): string
    {
        $sesi = DB::table($this->table)
            ->where('id', $id_sesi)
            ->first(['is_ada_tugas', 'waktu_akhir_pengumpulan_tugas']);
        if ($sesi->is_ada_tugas != 'Y') {
            return "Tidak ada tugas";
        } else if ($sesi->is_ada_tugas == 'Y') {
            $tugas = Tugas::where('id_sesi', $id_sesi)
                ->where('id_users', auth()->user()->id)
                ->first(['file_tugas']);
            if ($tugas != null) {
                return "Telah mengumpulkan";
            }
            return "Belum mengumpulkan";
        }
    }


    public function status_presensi($id_sesi): bool
    {
        $presensi = Presensi::where('id_sesi', $id_sesi)
            ->where('id_users', auth()->user()->id)
            ->first(['status_presensi']);
        if (($presensi->status_presensi ?? '') == 'Y') {
            return true;
        }
        return false;
    }
}
