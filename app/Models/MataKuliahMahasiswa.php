<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MataKuliahMahasiswa extends Model
{
    use HasFactory;
    protected $table = "mata_kuliah_mahasiswa";
    protected $guarded = [];

    public function mata_kuliah()
    {
        return $this->hasOne(MataKuliah::class, 'id', 'id_mata_kuliah');
    }
}
