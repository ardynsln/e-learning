<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('kelola_mata_kuliah')->group(function() {
    Route::get('/', 'KelolaMataKuliahController@index');
    Route::get('add', 'KelolaMataKuliahController@create');
    Route::post('store', 'KelolaMataKuliahController@store');
    Route::get('edit/{id}', 'KelolaMataKuliahController@edit');
    Route::post('update', 'KelolaMataKuliahController@update');
    Route::get('remove/{id}', 'KelolaMataKuliahController@destroy');
});
