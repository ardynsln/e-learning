@extends('template.backend')
@section('title', $title ?? '-')

@section('content')
<!-- container opened -->
<div class="container">

    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div>
            <div>
                <h4>{{ $title ?? '-'}}</h4>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1">
                    @if(isset($breadcrumb))
                    @foreach($breadcrumb as $i => $br)
                    @if(($i + 1) == count($breadcrumb))
                    <li class="breadcrumb-item active">{{ $br['title'] ?? '-' }}</li>
                    @else
                    <li class="breadcrumb-item">
                        <a href="{{ $br['link'] ?? '#' }}">{{ $br['title'] ?? '-' }}</a>
                    </li>
                    @endif
                    @endforeach
                    @endif
                </ol>
            </nav>
        </div>
    </div>
    <!-- /breadcrumb -->

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            @if (session('message') != null)
            <div class="alert alert-success">
                {{ session('message') ?? '' }}
            </div>
            <br>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h5>{{ $title ?? '-'}}</h5>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form method="POST" action="{{ url('kelola_mata_kuliah/update') }}">
                        <input type="hidden" name="id" value="{{ $data->id ?? '' }}">
                        @csrf
                        <div class="">
                            <div class="form-group">
                                <label for="kode_matkul">Kode Mata Kuliah</label>
                                <input type="text" class="form-control" id="kode_matkul" value="{{ $data->kode_matkul ?? old('kode_matkul') }}" name="kode_matkul">
                            </div>
                            <div class="form-group">
                                <label for="nama_matkul">Nama Mata Kuliah</label>
                                <input type="text" class="form-control" id="nama_matkul" value="{{ $data->nama_matkul ?? old('nama_matkul') }}" name="nama_matkul">
                            </div>
                            <div class="form-group">
                                <label for="id_ruangan_kelas">Ruang Kelas</label>
                                <select class="form-control select2" name="id_ruangan_kelas" required>
                                    <option value="">PILIH</option>
                                    @foreach($rungaKelases as $rk)
                                    <option value="{{ $rk->id }}" {{ $data->id_ruangan_kelas == $rk->id ? 'selected' : '' }}>
                                        {{ $rk->nama }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="id_dosen">Dosen Yang Mengajar</label>
                                <select class="form-control select2" name="id_dosen" required>
                                    <option value="">PILIH</option>
                                    @foreach($dosens as $d)
                                    <option value="{{ $d->id }}" {{ $data->id_dosen == $d->id ? 'selected' : '' }}>
                                        {{ $d->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tgl_awal">Tgl Awal Semester</label>
                                        <input type="date" class="form-control" id="tgl_awal" value="{{ $data->tgl_awal ?? old('tgl_awal') }}" name="tgl_awal">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tgl_akhir">Tgl Akhir Semester</label>
                                        <input type="date" class="form-control" id="tgl_akhir" value="{{ $data->tgl_akhir ?? old('tgl_akhir') }}" name="tgl_akhir">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="id_hari">Hari Pembelajaran</label>
                                        <select class="form-control select2" name="id_hari" required>
                                            <option value="">PILIH</option>
                                            @foreach($haries as $h)
                                            <option value="{{ $h->id }}" {{ $data->id_hari == $h->id ? 'selected' : '' }}>
                                                {{ $h->nama }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="jam_awal">Jam Awal Pembelajaran</label>
                                        <input type="time" class="form-control" id="jam_awal" value="{{ $data->jam_awal ?? old('jam_awal') }}" name="jam_awal">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="jam_akhir">Jam Akhir Pembelajaran</label>
                                        <input type="time" class="form-control" id="jam_akhir" value="{{ $data->jam_akhir ?? old('jam_akhir') }}" name="jam_akhir">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input1">Mahasiswa</label>
                                <select class="form-control select2" name="mahasiswas[]" multiple="multiple" required>
                                    <option value="">PILIH</option>
                                    @foreach($mahasiswas as $m)

                                    @php $selected = ''; @endphp
                                    @foreach($mahasiswaSelected as $sel)
                                    @if($sel->id_mahasiwa == $m->id)
                                    @php $selected = 'selected'; @endphp
                                    @endif
                                    @endforeach

                                    <option value="{{ $m->id }}" {{ $selected }}>
                                        {{ $m->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <center>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{ url('kelola_mata_kuliah') }}" class="btn btn-outline-danger">Kembali</a>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });

    if ("{{ $data->id ?? '' }}" != '') {
        $("#password").attr("autocomplete", "off");
    }
</script>
@endsection