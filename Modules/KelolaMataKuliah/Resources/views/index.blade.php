@extends('template.backend')
@section('title', $title ?? '-')

@section('content')
<!-- container opened -->
<div class="container">

    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div>
            <div>
                <h4>{{ $title ?? '-' }}</h4>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1">
                    @if(isset($breadcrumb))
                    @foreach($breadcrumb as $i => $br)
                    @if(($i + 1) == count($breadcrumb))
                    <li class="breadcrumb-item active">{{ $br['title'] ?? '-' }}</li>
                    @else
                    <li class="breadcrumb-item">
                        <a href="{{ $br['link'] ?? '#' }}">{{ $br['title'] ?? '-' }}</a>
                    </li>
                    @endif
                    @endforeach
                    @endif
                </ol>
            </nav>
        </div>
        <div class="" style="padding-top: 10px; text-align: right">
            <a href="{{ url('kelola_mata_kuliah/add') }}" class="btn btn-primary">Tambah</a>
        </div>
    </div>
    <!-- /breadcrumb -->

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h5>List Mata Kuliah</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 20px;">#</th>
                                    <th>Kode</th>
                                    <th>Mata Kuliah</th>
                                    <th>Ruang Kelas
                                    <th>Nama Dosen</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($matakuliahs as $key => $mk)
                                <tr>
                                    <td>{{ ($key + 1) }}</td>
                                    <td>{{ $mk->kode_matkul }}</td>
                                    <td>{{ $mk->nama_matkul }}</td>
                                    <td>{{ $mk->ruangKelas->kode_ruangan ?? '-' }} - {{ $mk->ruangKelas->nama ?? '-' }}</td>
                                    <td>{{ $mk->dosen->name ?? '-' }}</td>
                                    <td class="text-center">
                                        <a href="{{ url('kelola_mata_kuliah/edit/'.$mk->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                        <a href="{{ url('kelola_mata_kuliah/remove/'.$mk->id) }}" onclick="return confirm('Yakin hapus')" class="btn btn-danger btn-sm">Hapus</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
@endsection