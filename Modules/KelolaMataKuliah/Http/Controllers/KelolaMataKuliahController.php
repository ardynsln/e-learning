<?php

namespace Modules\KelolaMataKuliah\Http\Controllers;

use App\Models\Hari;
use App\Models\MataKuliah;
use App\Models\MataKuliahMahasiswa;
use App\Models\RuangKelas;
use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class KelolaMataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('kelolamatakuliah::index', [
            'title'      => 'Kelola Mata Kuliah',
            'matakuliahs'      => MataKuliah::all(),
            'breadcrumb' => [
                ['title' => 'Home', 'link' => ''],
                ['title' => 'Kelola Mata Kuliah', 'link' => url('kelola_mata_kuliah')]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('kelolamatakuliah::create', [
            'title'         => 'Tambah Mata Kuliah',
            'rungaKelases'  => RuangKelas::all(),
            'dosens'        => User::where('role_code', 'dosen')->get(),
            'mahasiswas'    => User::where('role_code', 'mahasiswa')->get(),
            'haries'        => Hari::all(),
            'breadcrumb' => [
                ['title' => 'Home', 'link' => ''],
                ['title' => 'Mata Kuliah', 'link' => url('kelola_mata_kuliah')],
                ['title' => 'Add', 'link' => url('kelola_mata_kuliah')]
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $id_mata_kuliah = DB::table('mata_kuliah')
            ->insertGetId([
                'kode_matkul'    => $request->kode_matkul,
                'nama_matkul'    => $request->nama_matkul,
                'id_hari'       => $request->id_hari,
                'tgl_awal'      => $request->tgl_awal,
                'tgl_akhir'     => $request->tgl_akhir,
                'jam_awal'      => $request->jam_awal,
                'jam_akhir'     => $request->jam_akhir,
                'id_dosen'      => $request->id_dosen,
                'id_ruangan_kelas'  => $request->id_ruangan_kelas,
            ]);

        if (count($request->mahasiswas) > 0) {
            foreach ($request->mahasiswas as $key => $id_mahasiswa) {
                DB::table('mata_kuliah_mahasiswa')
                    ->insertGetId([
                        'id_mata_kuliah'    => $id_mata_kuliah,
                        'id_mahasiwa'       => $id_mahasiswa,
                    ]);
            }
        }
        return redirect()->to(url('kelola_mata_kuliah'))->with('message_success', 'Berhasil disimpan');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('kelolamatakuliah::edit', [
            'title'         => 'Edit Mata Kuliah',
            'data'          => MataKuliah::find($id),
            'rungaKelases'  => RuangKelas::all(),
            'dosens'        => User::where('role_code', 'dosen')->get(),
            'mahasiswas'    => User::where('role_code', 'mahasiswa')->get(),
            'mahasiswaSelected' => MataKuliahMahasiswa::where('id_mata_kuliah', $id)->get(),
            'haries'        => Hari::all(),
            'breadcrumb' => [
                ['title' => 'Home', 'link' => ''],
                ['title' => 'Mata Kuliah', 'link' => url('kelola_mata_kuliah')],
                ['title' => 'Add', 'link' => url('kelola_mata_kuliah')]
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request)
    {
        DB::table('mata_kuliah')
            ->where('id', $request->id)
            ->update([
                'kode_matkul'    => $request->kode_matkul,
                'nama_matkul'    => $request->nama_matkul,
                'id_hari'       => $request->id_hari,
                'tgl_awal'      => $request->tgl_awal,
                'tgl_akhir'     => $request->tgl_akhir,
                'jam_awal'      => $request->jam_awal,
                'jam_akhir'     => $request->jam_akhir,
                'id_dosen'      => $request->id_dosen,
                'id_ruangan_kelas'  => $request->id_ruangan_kelas,
            ]);

        if (count($request->mahasiswas) > 0) {

            // HAPUS DATA LAMA
            DB::table('mata_kuliah_mahasiswa')
                ->where('id_mata_kuliah', $request->id)
                ->delete();

            // INSERT DATA BARU
            foreach ($request->mahasiswas as $id_mahasiswa) {
                DB::table('mata_kuliah_mahasiswa')
                    ->insertGetId([
                        'id_mata_kuliah'    => $request->id,
                        'id_mahasiwa'       => $id_mahasiswa,
                    ]);
            }
        }
        return redirect()->to(url('kelola_mata_kuliah'))->with('message_success', 'Berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        DB::table('mata_kuliah')
            ->where('id', $id)
            ->delete();

        DB::table('mata_kuliah_mahasiswa')
            ->where('id_mata_kuliah', $id)
            ->delete();
        return redirect()->to(url('kelola_mata_kuliah'))->with('message_success', 'Berhasil dihapus');
    }
}
