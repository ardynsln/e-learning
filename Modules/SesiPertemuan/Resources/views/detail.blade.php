@extends('template.backend')
@section('title', $title ?? '-')

@section('content')
<!-- container opened -->
<div class="container">

    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div>
            <div>
                <h4>{{ $title ?? '-' }}</h4>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1">
                    @if(isset($breadcrumb))
                    @foreach($breadcrumb as $i => $br)
                    @if(($i + 1) == count($breadcrumb))
                    <li class="breadcrumb-item active">{{ $br['title'] ?? '-' }}</li>
                    @else
                    <li class="breadcrumb-item">
                        <a href="{{ $br['link'] ?? '#' }}">{{ $br['title'] ?? '-' }}</a>
                    </li>
                    @endif
                    @endforeach
                    @endif
                </ol>
            </nav>
        </div>
        <div class="" style="padding-top: 10px; text-align: right">
            <a href="{{ url('sesi_pertemuan/create/'.$matkul->id) }}" class="btn btn-primary">Tambah Pertemuan</a>
        </div>
    </div>
    <!-- /breadcrumb -->

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <div class="alert alert-info" role="alert">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 120px">Mata Kuliah</td>
                                <td style="width: 30px">:</td>
                                <td>
                                    {{ $matkul->kode_matkul }} - {{ $matkul->nama_matkul }}
                                </td>
                            </tr>
                            <tr>
                                <td>Hari Pertemuan</td>
                                <td>:</td>
                                <td>
                                    {{ $matkul->hari->nama }}
                                </td>
                            </tr>
                            <tr>
                                <td>Jam Perkuliahan</td>
                                <td>:</td>
                                <td>
                                    {{ date('H:i', strtotime($matkul->jam_awal)) }} - {{ date('H:i', strtotime($matkul->jam_akhir)) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 150px;">Pertemuan Ke</th>
                                    <th>Deskripsi</th>
                                    <th>Tanggal</th>
                                    <th>Presensi</th>
                                    <th>Materi</th>
                                    <th>Ada Tugas</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($sesies) > 0)
                                @foreach($sesies as $index => $sesi)
                                <tr>
                                    <td>Pertemuan {{ $index + 1}}</td>
                                    <td>{{ $sesi->nama }}</td>
                                    <td>{{ date('d/m/Y', strtotime($sesi->waktu_awal_presensi))}}</td>
                                    <td></td>
                                    <td>
                                        @if($sesi->file_materi != '')
                                        <a href="{{ url($sesi->file_materi) }}" target="_blank"><span class="fa fa-download"></span> Download</a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($sesi->is_ada_tugas == 'Y')
                                        Ya
                                        @else
                                        Tidak Ada Tugas
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="" class="btn btn-primary btn-sm">Detail</a>
                                        <a href="" class="btn btn-danger btn-sm" onclick="return confirm('Yakin hapus?')">Hapus</a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="20">Belum ada data.</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection