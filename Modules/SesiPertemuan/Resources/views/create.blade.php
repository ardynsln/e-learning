@extends('template.backend')
@section('title', $title ?? '-')

@section('content')
<!-- container opened -->
<div class="container">

    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div>
            <div>
                <h4>{{ $title ?? '-'}}</h4>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1">
                    @if(isset($breadcrumb))
                    @foreach($breadcrumb as $i => $br)
                    @if(($i + 1) == count($breadcrumb))
                    <li class="breadcrumb-item active">{{ $br['title'] ?? '-' }}</li>
                    @else
                    <li class="breadcrumb-item">
                        <a href="{{ $br['link'] ?? '#' }}">{{ $br['title'] ?? '-' }}</a>
                    </li>
                    @endif
                    @endforeach
                    @endif
                </ol>
            </nav>
        </div>
    </div>
    <!-- /breadcrumb -->

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            @if (session('message') != null)
            <div class="alert alert-success">
                {{ session('message') ?? '' }}
            </div>
            <br>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h5>{{ $title ?? '-'}}</h5>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form method="POST" action="{{ url('sesi_pertemuan/store') }}" enctype="multipart/form-data">
                        <input type="hidden" name="id_matkul" value="{{ $matkul->id ?? '' }}">
                        <input type="hidden" name="id" value="{{ $data->id ?? '' }}">
                        @csrf
                        <div class="">
                            <div class="form-group">
                                <label for="nama">Deskripsi</label>
                                <input type="text" class="form-control" id="nama" value="{{ $data->nama ?? old('nama') }}" name="nama">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="waktu_awal_presensi">Waktu Awal Presensi</label>
                                        <input type="datetime-local" class="form-control" id="waktu_awal_presensi" value="{{ $data->waktu_awal_presensi ?? old('waktu_awal_presensi') }}" name="waktu_awal_presensi">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="waktu_akhir_presensi">Waktu Akhir Presensi</label>
                                        <input type="datetime-local" class="form-control" id="waktu_akhir_presensi" value="{{ $data->waktu_akhir_presensi ?? old('waktu_akhir_presensi') }}" name="waktu_akhir_presensi">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="file_materi">File Materi</label>
                                <input type="file" class="form-control" id="file_materi" value="{{ $data->file_materi ?? old('file_materi') }}" name="file_materi">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input1">Ada Tugas?</label>
                                        <select class="form-control" name="is_ada_tugas" required>
                                            <option value="">PILIH</option>
                                            <option value="N">Tidak Ada Tugas</option>
                                            <option value="Y">Ada Tugas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input1">Deskripsi Tugas</label>
                                <textarea class="form-control" name="deskripsi_tugas"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="waktu_akhir_pengumpulan_tugas">Waktu Akhir Pengumpulan Tugas</label>
                                <input type="datetime-local" class="form-control" id="waktu_akhir_pengumpulan_tugas" value="{{ $data->waktu_akhir_pengumpulan_tugas ?? old('waktu_akhir_pengumpulan_tugas') }}" name="waktu_akhir_pengumpulan_tugas">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ url('sesi_pertemuan/detail/'.$matkul->id) }}" class="btn btn-outline-danger">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    if ("{{ $data->id ?? '' }}" != '') {
        $("#password").attr("autocomplete", "off");
    }
</script>
@endsection