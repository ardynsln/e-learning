<?php

namespace Modules\SesiPertemuan\Http\Controllers;

use App\Models\MataKuliah;
use App\Models\Sesi;
use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class SesiPertemuanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $sesies = MataKuliah::select(['*'])
            ->where('id_dosen', '=', auth()->user()->id)
            ->get();

        return view('sesipertemuan::index',  [
            'title' => 'Sesi Pertemuan',
            'sesies'  => $sesies,
            'breadcrumb' => [
                ['title' => 'Home', 'link' => ''],
                ['title' => 'Sesi Pertemuan', 'link' => url('sesi_pertemuan')]
            ]
        ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function detail($id_matkul)
    {
        $matkul = MataKuliah::find($id_matkul);
        $sesies   = Sesi::where('id_mata_kuliah', $id_matkul)->get();
        return view('sesipertemuan::detail', [
            'title'     => 'Sesi Pertemuan ' . $matkul->kode_matkul . ' - ' . $matkul->nama_matkul,
            'matkul'    => $matkul,
            'sesies'    => $sesies,
            'breadcrumb' => [
                ['title' => 'Home', 'link' => ''],
                ['title' => 'Sesi Pertemuan', 'link' => url('sesi_pertemuan')],
                ['title' => $matkul->kode_matkul . ' - ' . $matkul->nama_matkul, 'link' => url('sesi_pertemuan/detail/' . $matkul->id)]
            ]
        ]);
    }


    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create($id_matkul)
    {
        $matkul = MataKuliah::find($id_matkul);
        return view('sesipertemuan::create', [
            'title'     => 'Tambah Pertemuan',
            'matkul'    => $matkul,
            'breadcrumb' => [
                ['title' => 'Home', 'link' => ''],
                ['title' => 'Sesi Pertemuan', 'link' => url('sesi_pertemuan')],
                ['title' => $matkul->kode_matkul . ' - ' . $matkul->nama_matkul, 'link' => url('sesi_pertemuan/detail/' . $matkul->id)],
                ['title' => 'Tambah Pertemuan', 'link' => url('sesi_pertemuan/detail/' . $matkul->id)]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        // upload file
        $file = $request->file('file_materi');
        $tujuan_upload = 'uploads/file_materi';
        $file->move($tujuan_upload, $file->getClientOriginalName());

        $data = [
            'id_mata_kuliah'            => $request->id_matkul,
            'nama'                      => $request->nama,
            'waktu_awal_presensi'       => $request->waktu_awal_presensi,
            'waktu_akhir_presensi'      => $request->waktu_akhir_presensi,
            'is_ada_tugas'              => $request->is_ada_tugas,
            'deskripsi_tugas'           => $request->deskripsi_tugas,
            'waktu_akhir_pengumpulan_tugas' => $request->waktu_akhir_pengumpulan_tugas,
            'file_materi'                   => $tujuan_upload . '/' . $file->getClientOriginalName(),
        ];
        Sesi::insert($data);
        return redirect()->to(url('sesi_pertemuan/detail/' . $request->id_matkul))->with('message_success', 'Berhasil disimpan');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('sesipertemuan::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
