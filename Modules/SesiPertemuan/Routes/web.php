<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('sesi_pertemuan')->group(function () {
    Route::get('/', 'SesiPertemuanController@index');
    Route::get('detail/{id}', 'SesiPertemuanController@detail');
    Route::get('create/{id}', 'SesiPertemuanController@create');
    Route::post('store', 'SesiPertemuanController@store');
});
