<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('mata_kuliah')->group(function () {
    Route::get('/', 'MataKuliahController@index');
    Route::get('sesi/{id_matkul}', 'MataKuliahController@sesi');
    Route::get('detail/{id_matkul}/{id_sesi}', 'MataKuliahController@detail');
    Route::post('submit_presensi', 'MataKuliahController@submit_presensi');
    Route::post('upload_materi', 'MataKuliahController@upload_materi');
});
