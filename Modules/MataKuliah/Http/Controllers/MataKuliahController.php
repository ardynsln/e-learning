<?php

namespace Modules\MataKuliah\Http\Controllers;

use App\Models\MasterPresensi;
use App\Models\MataKuliah;
use App\Models\MataKuliahMahasiswa;
use App\Models\Presensi;
use App\Models\Sesi;
use App\Models\Tugas;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $mataKuliah = MataKuliahMahasiswa::select(['*'])
            ->where('mata_kuliah_mahasiswa.id_mahasiwa', auth()->user()->id)
            ->get();

        return view('matakuliah::index',  [
            'title'         => 'Mata Kuliah',
            'mataKuliah'    => $mataKuliah,
            'breadcrumb' => [
                ['title' => 'Home', 'link' => ''],
                ['title' => 'Mata Kuliah', 'link' => url('mata_kuliah')],
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function sesi($id_matkul)
    {
        $matkul = MataKuliah::find($id_matkul);
        $sesies   = Sesi::where('id_mata_kuliah', $id_matkul)->get();
        return view('matakuliah::sesi', [
            'title'         => 'Mata Kuliah',
            'matkul'        => $matkul,
            'sesies'        => $sesies,
            'breadcrumb' => [
                ['title' => 'Home', 'link' => ''],
                ['title' => 'Sesi Perkuliahan', 'link' => url('mata_kuliah')],
            ],
        ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function detail($id_matkul, $id_sesi)
    {
        $matkul         = MataKuliah::find($id_matkul);
        $sesi           = Sesi::find($id_sesi);
        $materPresensi  = MasterPresensi::all();
        $presensi       = Presensi::where('id_users', auth()->user()->id)
            ->where('id_sesi', $id_sesi)
            ->first();

        return view('matakuliah::detail', [
            'title'         => 'Mata Kuliah',
            'matkul'        => $matkul,
            'sesi'          => $sesi,
            'presensi'      => $presensi,
            'master_presensi'   => $materPresensi,
            'breadcrumb' => [
                ['title' => 'Home', 'link' => ''],
                ['title' => 'Sesi Perkuliahan', 'link' => url('mata_kuliah')],
            ],
        ]);
    }


    public function submit_presensi(Request $request)
    {
        DB::table('presensis')
            ->where('id_sesi', $request->id_sesi)
            ->where('id_users', auth()->user()->id)
            ->delete();

        $data = [
            'id_sesi'   => $request->id_sesi,
            'id_users'  => auth()->user()->id,
            'id_master_presensi'    => $request->id_mater_presensi,
            'status_presensi'       => 'Y',
            'deskripsi'             => "Self Record",
            'created_at'            => date('Y-m-d H:i:s')
        ];
        Presensi::insert($data);
        return redirect()->back();
    }


    public function upload_materi(Request $request)
    {
        // upload file
        $file = $request->file('file');
        $tujuan_upload = 'uploads/file_tugas';
        if ($file != null) {
            $file->move($tujuan_upload, $file->getClientOriginalName());
        }

        DB::table('tugas')
            ->where('id_sesi', $request->id_sesi)
            ->where('id_users', auth()->user()->id)
            ->delete();

        $data = [
            'id_sesi'   => $request->id_sesi,
            'id_users'  => auth()->user()->id,
            'deskripsi'             => $request->deskripsi,
            'created_at'            => date('Y-m-d H:i:s')
        ];
        if ($file != null) {
            $data['file_tugas'] = $tujuan_upload . '/' . $file->getClientOriginalName();
        }
        Tugas::insert($data);
        return redirect()->back();
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('matakuliah::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
