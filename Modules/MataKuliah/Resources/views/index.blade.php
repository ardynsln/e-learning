@extends('template.backend')
@section('title', $title ?? '-')

@section('content')
<!-- container opened -->
<div class="container">

    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div>
            <div>
                <h4>{{ $title ?? '-' }}</h4>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1">
                    @if(isset($breadcrumb))
                    @foreach($breadcrumb as $i => $br)
                    @if(($i + 1) == count($breadcrumb))
                    <li class="breadcrumb-item active">{{ $br['title'] ?? '-' }}</li>
                    @else
                    <li class="breadcrumb-item">
                        <a href="{{ $br['link'] ?? '#' }}">{{ $br['title'] ?? '-' }}</a>
                    </li>
                    @endif
                    @endforeach
                    @endif
                </ol>
            </nav>
        </div>
    </div>
    <!-- /breadcrumb -->

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h5>List Mata Kuliah</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 20px;">#</th>
                                    <th>Mata Kuliah</th>
                                    <th>Ruang Kelas</th>
                                    <th>Nama Dosen</th>
                                    <th>Waktu Perkuliahan</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($mataKuliah ?? [] as $key => $mk)
                                <tr>
                                    <td>{{ ($key + 1) }}</td>
                                    <td>{{ $mk->mata_kuliah->kode_matkul }} - {{ $mk->mata_kuliah->nama_matkul }}</td>
                                    <td>{{ $mk->mata_kuliah->ruangKelas->kode_ruangan }} - {{ $mk->mata_kuliah->ruangKelas->nama }}</td>
                                    <td>{{ $mk->mata_kuliah->dosen->name }}</td>
                                    <td>
                                        {{ $mk->mata_kuliah->hari->nama }},
                                        {{ date('H:i', strtotime($mk->mata_kuliah->jam_awal)) }} -
                                        {{ date('H:i', strtotime($mk->mata_kuliah->jam_akhir)) }}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ url('mata_kuliah/sesi/'.$mk->id_mata_kuliah) }}" class="btn btn-primary btn-sm">Detail</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>

</script>
@endsection