@extends('template.backend')
@section('title', $title ?? '-')

@section('content')
<!-- container opened -->
<div class="container">

    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div>
            <div>
                <h4>{{ $title ?? '-' }}</h4>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1">
                    @if(isset($breadcrumb))
                    @foreach($breadcrumb as $i => $br)
                    @if(($i + 1) == count($breadcrumb))
                    <li class="breadcrumb-item active">{{ $br['title'] ?? '-' }}</li>
                    @else
                    <li class="breadcrumb-item">
                        <a href="{{ $br['link'] ?? '#' }}">{{ $br['title'] ?? '-' }}</a>
                    </li>
                    @endif
                    @endforeach
                    @endif
                </ol>
            </nav>
        </div>
    </div>
    <!-- /breadcrumb -->

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <div class="alert alert-info" role="alert">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 120px">Mata Kuliah</td>
                                <td style="width: 30px">:</td>
                                <td>
                                    {{ $matkul->kode_matkul }} - {{ $matkul->nama_matkul }}
                                </td>
                            </tr>
                            <tr>
                                <td>Hari Pertemuan</td>
                                <td>:</td>
                                <td>
                                    {{ $matkul->hari->nama }}
                                </td>
                            </tr>
                            <tr>
                                <td>Jam Perkuliahan</td>
                                <td>:</td>
                                <td>
                                    {{ date('H:i', strtotime($matkul->jam_awal)) }} - {{ date('H:i', strtotime($matkul->jam_akhir)) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 150px;">Pertemuan Ke</th>
                                    <th>Deskripsi</th>
                                    <th class="text-center">Presensi</th>
                                    <th class="text-center">Tugas</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($sesies) > 0)
                                @foreach($sesies as $index => $sesi)
                                <tr>
                                    <td>Pertemuan {{ $index + 1}}</td>
                                    <td>{{ $sesi->nama }}</td>
                                    <td class="text-center">
                                        @if($sesi->status_presensi($sesi->id))
                                        <span class="fa fa-check"></span>
                                        @else
                                        ?
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        {{ $sesi->status_tugas($sesi->id) }}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ url('mata_kuliah/detail/'.$sesi->id_mata_kuliah.'/'.$sesi->id) }}" class="btn btn-primary btn-sm">Detail</a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="20">Belum ada data.</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>

</script>
@endsection