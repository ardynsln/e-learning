@extends('template.backend')
@section('title', $title ?? '-')

@section('content')
<!-- container opened -->
<div class="container">

    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div>
            <div>
                <h4>{{ $title ?? '-' }}</h4>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1">
                    @if(isset($breadcrumb))
                    @foreach($breadcrumb as $i => $br)
                    @if(($i + 1) == count($breadcrumb))
                    <li class="breadcrumb-item active">{{ $br['title'] ?? '-' }}</li>
                    @else
                    <li class="breadcrumb-item">
                        <a href="{{ $br['link'] ?? '#' }}">{{ $br['title'] ?? '-' }}</a>
                    </li>
                    @endif
                    @endforeach
                    @endif
                </ol>
            </nav>
        </div>
    </div>
    <!-- /breadcrumb -->

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-body">

                    <table style="width: 100%">
                        <tr>
                            <td style="width: 120px">Mata Kuliah</td>
                            <td style="width: 30px">:</td>
                            <td>
                                {{ $matkul->kode_matkul }} - {{ $matkul->nama_matkul }}
                            </td>
                        </tr>
                        <tr>
                            <td>Hari Pertemuan</td>
                            <td>:</td>
                            <td>
                                {{ $matkul->hari->nama }}
                            </td>
                        </tr>
                        <tr>
                            <td>Jam Perkuliahan</td>
                            <td>:</td>
                            <td>
                                {{ date('H:i', strtotime($matkul->jam_awal)) }} - {{ date('H:i', strtotime($matkul->jam_akhir)) }}
                            </td>
                        </tr>
                        <tr>
                            <td>Sesi</td>
                            <td>:</td>
                            <td>
                                {{ $sesi->nama }}
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Presensi</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div>
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 120px">Status Presensi</td>
                                        <td style="width: 30px">:</td>
                                        <td>
                                            @if(!empty($presensi))
                                            {{ $presensi->master_presensi->nama }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Waktu Presensi</td>
                                        <td>:</td>
                                        <td>
                                            @if(!empty($presensi))
                                            {{ date('d/m/Y H:i', strtotime($presensi->created_at)) }}
                                            @else
                                            ?
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tipe</td>
                                        <td>:</td>
                                        <td>
                                            @if(!empty($presensi))
                                            {{ $presensi->deskripsi }}
                                            @else
                                            ?
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            @if(empty($presensi))
                            <br>
                            <form method="POST" action="{{ url('mata_kuliah/submit_presensi') }}">
                                @csrf
                                <input type="hidden" name="id_matkul" value="{{ $matkul->id }}">
                                <input type="hidden" name="id_sesi" value="{{ $sesi->id }}">
                                <div class="form-group">
                                    <label for="input1">Input Presensi</label>
                                    <select class="form-control" name="id_mater_presensi" required>
                                        <option value="">PILIH</option>
                                        @foreach($master_presensi as $pre)
                                        <option value="{{ $pre->id }}">{{ $pre->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Simpan Presensi</button>
                                </div>
                            </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($sesi->file_materi != '')
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Materi</h5>
                </div>
                <div class="card-body">
                    <p>{{ $sesi->nama }}</p>
                    <a target="_blank" class="btn btn-primary" href="{{ url($sesi->file_materi) }}"><span class="fa fa-download"></span> Download Materi</a>
                </div>
            </div>
        </div>
        @endif

        @if($sesi->is_ada_tugas == 'Y')
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5>Tugas</h5>
                </div>
                <div class="card-body">
                    <p>{{ $sesi->deskripsi_tugas }}</p>
                    <div style="margin-bottom: 10px">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 180px">Tenggat Pengumpulan</td>
                                <td style="width: 30px">:</td>
                                <td>
                                    {{ date('d/m/Y H:i', strtotime($sesi->waktu_akhir_pengumpulan_tugas)) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <form method="POST" enctype="multipart/form-data" action="{{ url('mata_kuliah/upload_materi') }}">
                        @csrf
                        <input type="hidden" name="id_matkul" value="{{ $matkul->id }}">
                        <input type="hidden" name="id_sesi" value="{{ $sesi->id }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="input1">Deskripsi</label>
                                    <textarea class="form-control" name="deskripsi"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="input1">Upload Tugas</label>
                                    <input type="file" name="file" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Serahkan Tugas</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection

@section('script')
<script>

</script>
@endsection