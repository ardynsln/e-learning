<?php

namespace Modules\Mahasiswa\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data = DB::table("users")->where("role_code", "mahasiswa")->get();

        return view('mahasiswa::index', [
            'title' => 'Kelola Mahasiswa',
            "data" => $data,
            'breadcrumb' => [
                ['title' => 'Home', 'link' => ''],
                ['title' => 'Kelola Mahasiswa', 'link' => route('user.index')]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view(
            'mahasiswa::create',
            [
                'title' => 'Tambah Mahasiswa',
                'breadcrumb' => [
                    ['title' => 'Home', 'link' => ''],
                    ['title' => 'User Managment', 'link' => route('user.index')]
                ]
            ]
        );
    }

    public function simpan_tambah(Request $request)
    {
        // dd($_POST);
        DB::table("users")->insert([
            "name" => $request->name,
            "email" => $request->email,
            "password" => password_hash($request->password, PASSWORD_DEFAULT),
            "no_telpon" => $request->no_telpon,
            "nim" => $request->nim,
            "role_code" => "mahasiswa"
        ]);
        return redirect()->to(url("mahasiswa"));
    }

}