@extends('template.backend')
@section('title', $title ?? '-')

@section('content')
<!-- container opened -->
<div class="container">

    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div>
            <div>
                <h4>{{ $title ?? '-'}}</h4>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1">
                    @if(isset($breadcrumb))
                    @foreach($breadcrumb as $i => $br)
                    @if(($i + 1) == count($breadcrumb))
                    <li class="breadcrumb-item active">{{ $br['title'] ?? '-' }}</li>
                    @else
                    <li class="breadcrumb-item">
                        <a href="{{ $br['link'] ?? '#' }}">{{ $br['title'] ?? '-' }}</a>
                    </li>
                    @endif
                    @endforeach
                    @endif
                </ol>
            </nav>
        </div>
    </div>
    <!-- /breadcrumb -->

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            @if (session('message') != null)
            <div class="alert alert-success">
                {{ session('message') ?? '' }}
            </div>
            <br>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h5>{{ $title ?? '-'}}</h5>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form method="POST" action="{{ url('mahasiswa/store') }}">
                        <input type="hidden" name="id" value="{{ $data->id ?? '' }}">
                        @csrf
                        <div class="">
                            <div class="form-group">
                                <label for="input1">Nama Lengkap</label>
                                <input type="text" class="form-control" id="input1" value="{{ $data->name ?? old('name') }}" name="name">
                            </div>
                            <div class="form-group">
                                <label for="input1">Email</label>
                                <input type="text" class="form-control" id="email" value="{{ $data->email ?? old('email') }}" name="email">
                            </div>
                            <div class="form-group">
                                <label for="input1">Password</label>
                                <input type="text" class="form-control" id="password" name="password">
                            </div>
                            <div class="form-group">
                                <label for="input1">No Telpon</label>
                                <input type="number" class="form-control" id="no_telpon" value="{{ $data->no_telpon ?? old('no_telpon') }}" name="no_telpon">
                            </div>
                            <div class="form-group">
                                <label for="input1">NIM</label>
                                <input type="number" class="form-control" id="nim" value="{{ $data->nim ?? old('nim') }}" name="nim">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ url('mahasiswa') }}" class="btn btn-outline-danger">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    if ("{{ $data->id ?? '' }}" != '') {
        $("#password").attr("autocomplete", "off");
    }
</script>
@endsection