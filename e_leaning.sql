-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.24-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for e_learning
DROP DATABASE IF EXISTS `e_learning`;
CREATE DATABASE IF NOT EXISTS `e_learning` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `e_learning`;

-- Dumping structure for table e_learning.failed_jobs
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e_learning.failed_jobs: ~0 rows (approximately)

-- Dumping structure for table e_learning.menus
DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parrent_menu_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menus_parrent_menu_id_index` (`parrent_menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e_learning.menus: ~7 rows (approximately)
INSERT INTO `menus` (`id`, `parrent_menu_id`, `name`, `link`, `urutan`, `created_at`, `updated_at`) VALUES
	(1, 0, 'Dashboard', 'dashboard', 1, NULL, NULL),
	(2, 0, 'Menu Management', 'menu', 2, NULL, NULL),
	(3, 0, 'Role Management', 'role', 3, NULL, NULL),
	(4, 0, 'User Management', 'user', 4, NULL, NULL),
	(5, 0, 'Kelola Mata Kuliah', 'kelola_mata_kuliah', 1, '2023-01-07 06:14:35', '2023-01-07 06:14:35'),
	(6, 0, 'Sesi Pertemuan', 'sesi_pertemuan', 1, '2023-01-07 06:15:27', '2023-01-07 06:15:27'),
	(7, 0, 'Mata Kuliah', 'mata_kuliah', 1, '2023-01-07 06:15:53', '2023-01-07 06:15:53');

-- Dumping structure for table e_learning.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e_learning.migrations: ~9 rows (approximately)
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(5, '2022_02_07_135603_create_roles_table', 1),
	(6, '2022_02_07_135829_add_role_code_to_user', 1),
	(7, '2022_02_10_141322_create_menus_table', 1),
	(8, '2022_02_22_104422_create_role_accesses_table', 1),
	(9, '2022_02_23_001534_add_access_to_role_accesses', 1);

-- Dumping structure for table e_learning.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e_learning.password_resets: ~0 rows (approximately)

-- Dumping structure for table e_learning.personal_access_tokens
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e_learning.personal_access_tokens: ~0 rows (approximately)

-- Dumping structure for table e_learning.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roles_role_code_index` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e_learning.roles: ~4 rows (approximately)
INSERT INTO `roles` (`id`, `role_code`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'superadmin', 'Super Admin', '2023-01-07 05:23:58', '2023-01-07 05:23:58'),
	(2, 'akademik', 'Admin / Akademik', '2023-01-07 06:13:18', '2023-01-07 06:13:18'),
	(3, 'dosen', 'DOSEN', '2023-01-07 06:13:29', '2023-01-07 06:13:29'),
	(4, 'mahasiswa', 'MAHASISWA', '2023-01-07 06:13:42', '2023-01-07 06:13:42');

-- Dumping structure for table e_learning.role_accesses
DROP TABLE IF EXISTS `role_accesses`;
CREATE TABLE IF NOT EXISTS `role_accesses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `access` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delete` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_accesses_role_code_index` (`role_code`),
  KEY `role_accesses_menu_id_index` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e_learning.role_accesses: ~10 rows (approximately)
INSERT INTO `role_accesses` (`id`, `role_code`, `menu_id`, `access`, `view`, `create`, `update`, `delete`, `created_at`, `updated_at`) VALUES
	(1, 'superadmin', 1, NULL, 'Y', 'Y', 'Y', 'Y', NULL, NULL),
	(2, 'superadmin', 2, NULL, 'Y', 'Y', 'Y', 'Y', NULL, NULL),
	(3, 'superadmin', 3, NULL, 'Y', 'Y', 'Y', 'Y', NULL, NULL),
	(4, 'superadmin', 4, NULL, 'Y', 'Y', 'Y', 'Y', NULL, NULL),
	(5, 'akademik', 1, NULL, NULL, NULL, NULL, NULL, '2023-01-07 06:16:06', '2023-01-07 06:16:06'),
	(6, 'akademik', 5, NULL, NULL, 'Y', 'Y', 'Y', '2023-01-07 06:16:09', '2023-01-07 06:16:12'),
	(7, 'dosen', 1, NULL, NULL, NULL, NULL, NULL, '2023-01-07 06:16:23', '2023-01-07 06:16:23'),
	(8, 'dosen', 6, NULL, NULL, 'Y', 'Y', 'Y', '2023-01-07 06:16:24', '2023-01-07 06:16:28'),
	(9, 'mahasiswa', 7, NULL, NULL, 'Y', 'Y', 'Y', '2023-01-07 06:17:05', '2023-01-07 06:17:08'),
	(10, 'mahasiswa', 1, NULL, NULL, NULL, NULL, NULL, '2023-01-07 06:17:10', '2023-01-07 06:17:10');

-- Dumping structure for table e_learning.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_code_index` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e_learning.users: ~4 rows (approximately)
INSERT INTO `users` (`id`, `role_code`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'superadmin', 'Super Admin', 'superadmin@mail.com', NULL, '$2y$10$4emX4RmrgEh2JQ3rbgeBHOw.sV2BREkLydkTKu.yRyVBF/IRT8e3e', NULL, '2023-01-07 05:25:11', '2023-01-07 05:25:11'),
	(2, 'akademik', 'Admin Akademik', 'akademik@mail.com', NULL, '$2y$10$vSOrTzz019osYGSKOybZW.55aTtxFuVgWGhqjq2hVk5bF8tve0XSi', NULL, '2023-01-07 06:18:00', '2023-01-07 06:18:00'),
	(3, 'dosen', 'Dosen A', 'dosena@mail.com', NULL, '$2y$10$4jxedwMbddIEGAJPOZkenuk7iaWQwojje0ixZaAAbIHmmkb9PDL3q', NULL, '2023-01-07 06:18:41', '2023-01-07 06:18:41'),
	(4, 'mahasiswa', 'Iqbal Nugraha', 'iqbalnugraha@mail.com', NULL, '$2y$10$.wZePC5GMDOKnexUf4JB2O7cxdGc4JRPkgYd4czCV1aqZWFGnySr6', NULL, '2023-01-07 06:19:16', '2023-01-07 06:19:16');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
